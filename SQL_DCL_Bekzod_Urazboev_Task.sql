CREATE USER rentaluser WITH PASSWORD 'rentalpassword';
GRANT CONNECT ON DATABASE your_database_name TO rentaluser;
GRANT SELECT ON customer TO rentaluser;
-- This is a conceptual representation. The switch to 'rentaluser' and test must be done via your database access tools or SQL client.

-- Assuming now operating as 'rentaluser'
SELECT * FROM customer;
CREATE ROLE rental;
GRANT rental TO rentaluser;
GRANT INSERT, UPDATE ON rental TO rental;
-- The following queries need to be executed as a user that is a member of the 'rental' group.

-- INSERT example (assuming suitable columns):
-- INSERT INTO rental (column1, column2) VALUES (value1, value2);

-- UPDATE example:
-- UPDATE rental SET column1 = new_value WHERE condition;
REVOKE INSERT ON rental FROM rental;
SELECT first_name, last_name FROM customer
JOIN rental ON customer.customer_id = rental.customer_id
JOIN payment ON customer.customer_id = payment.customer_id
GROUP BY customer.customer_id
HAVING COUNT(rental.rental_id) > 0 AND COUNT(payment.payment_id) > 0
LIMIT 1;
CREATE ROLE client_John_Doe NOLOGIN;
-- Enable RLS on tables
ALTER TABLE rental ENABLE ROW LEVEL SECURITY;
ALTER TABLE payment ENABLE ROW LEVEL SECURITY;

-- Create policies that limit access to customer data
CREATE POLICY rental_access ON rental USING (customer_id = get_customer_id_for_role(current_role));
CREATE POLICY payment_access ON payment USING (customer_id = get_customer_id_for_role(current_role));
-- Assume switched to 'client_John_Doe'

SELECT * FROM rental;
SELECT * FROM payment;
